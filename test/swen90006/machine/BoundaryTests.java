package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
	Machine m = new Machine();
	
	//Test valid case when the Ra in RET holds some value
	 @Test public void TestEC1()
	  {
	    final List<String> lines = readInstructions("examples/boundaryEC1.s");
	    m.execute(lines);
	  }
	//Test on points for register is valid
	  @Test public void TestEC2a()
	  {
		  final List<String> lines = readInstructions("examples/boundaryEC2a.s");
		  m.execute(lines);
	  }
	  
	//Test on points for register is valid
	  @Test public void TestEC2b()
	  {
		  final List<String> lines = readInstructions("examples/boundaryEC2b.s");
		  m.execute(lines);
	  }
	  
	  //Test on point for Rc is not equal to 0
	  @Test public void TestEC3a() 
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC3a.s");
		  m.execute(lines);
	 }
	  
	  //Test off point for Rc is 0
	  @Test public void TestEC3b()
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC3b.s");
		  m.execute(lines);//"no op" for dividend is 0, so there should throw no exception
	 }
	  
	  //Test off point for val is not equal to 1
	  @Test(expected = NoReturnValueException.class)
	  public void TestEC5() throws Throwable
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC5.s");  
		  m.execute(lines);
		  throw new NoReturnValueException();
	 }
	  
	  //Test on point for the val is the length of the program
	  @Test public void TestEC6() 
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC6.s");
		  m.execute(lines);
	 }
	  
	  //Test off point for val is greater than the length of the program
	  @Test(expected = NoReturnValueException.class)
	  
	  public void TestEC7() throws Throwable
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC7.s");  
		  m.execute(lines);
		  throw new NoReturnValueException();
	 }
	  
	  //Test on point for value belongs to {-65535,...,65535}
	  @Test public void TestEC10a() 
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC10a.s");
		  m.execute(lines);
	 }
	  //Test on point for value belongs to {-65535,...,65535}
	  @Test public void TestEC10b() 
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC10b.s");
		  m.execute(lines);
	 }
	  //Test off point for value belongs to {-65535,...,65535}
	  @Test(expected = InvalidInstructionException.class)
	  public void TestEC10c()  throws Throwable
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC10c.s");
		  m.execute(lines);
		  throw new InvalidInstructionException();
	 }
	  
	  //Test on point for pc value in LDR is valid
	  @Test public void TestEC11a() 
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC11a.s");
		  m.execute(lines);
	 }
	  //Test on point for pc value in LDR is valid
	  @Test public void TestEC11b() 
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC11b.s");
		  m.execute(lines);
	 }
	  //Test off point for pc value in LDR is valid
	  @Test(expected = InvalidInstructionException.class)
	  public void TestEC11c()  throws Throwable
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC11c.s");
		  m.execute(lines);
		  throw new InvalidInstructionException();
	 }
	  //Test on point for JZ instruction is valid
	  @Test public void TestEC13() 
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC13.s");
		  m.execute(lines);
	 }
	  
	  //Test off point for JZ instruction is valid
	  @Test(expected = NoReturnValueException.class)
	  public void TestEC14() throws Throwable
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC14.s");
		  m.execute(lines);
		  throw new NoReturnValueException();
	 }
	  
	  //Test on point for JZ instruction is valid with Ra is 0
	  @Test public void TestEC15() 
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC15.s");
		  m.execute(lines);
	 }
	  
	  //Test off point for invalid instruction with no instructions in the file
	  @Test(expected = ArrayIndexOutOfBoundsException.class)
	  public void TestEC17() throws Throwable
	 {
		  final List<String> lines = readInstructions("examples/boundaryEC17.s");
		  m.execute(lines);
		  throw new ArrayIndexOutOfBoundsException();
	 }

  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
