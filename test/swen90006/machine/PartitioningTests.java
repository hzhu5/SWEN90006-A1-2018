package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
	Machine m = new Machine();
  
//Test when the Ra in RET does not hold any value
  @Test public void TestEC1()
  {
    final List<String> lines = readInstructions("examples/ec1.s");
	  
    m.execute(lines);
    //if the first line is RET, the Ra is not assigned
    assertTrue(lines.get(0).substring(0, 3).equals("RET"));
  }
//Test when the register name is out of range
  @Test(expected = InvalidInstructionException.class)
   public void TestEC2() throws Throwable
  {
	  final List<String> lines = readInstructions("examples/ec2.s");
	  
	  m.execute(lines);
	  throw new InvalidInstructionException();
  }
  
  //Test when dividend is 0
  @Test public void TestEC3() throws Throwable
 {
	  final List<String> lines = readInstructions("examples/ec3.s");
	  
	  m.execute(lines);//"no op" for dividend is 0, so there should throw no exception
 }
  
  //Test valid input JMP with value is 1
  @Test public void TestEC5()
 {
	  final List<String> lines = readInstructions("examples/ec5.s");
	  m.execute(lines);
 }
  
  //Test valid input JMP with value is greater than 1
  @Test public void TestEC6()
 {
	  final List<String> lines = readInstructions("examples/ec6.s");
	  m.execute(lines);
 }
  
 //Test invalid input JMP with value is greater than the list length
  @Test(expected = NoReturnValueException.class)
  public void TestEC7() throws Throwable
 {
	  final List<String> lines = readInstructions("examples/ec7.s");  
	  m.execute(lines);
	  throw new NoReturnValueException();
 }
  
  //Test invalid input with no RET instruction included
 @Test(expected = NoReturnValueException.class)
 public void TestEC8() throws Throwable
 {
	  final List<String> lines = readInstructions("examples/ec8.s");  
	  m.execute(lines);
	  throw new NoReturnValueException();
 }
 
 //Test invalid MOV instruction with assign double number to register
@Test(expected = InvalidInstructionException.class)
public void TestEC10() throws Throwable
{
	  final List<String> lines = readInstructions("examples/ec10.s");  
	  m.execute(lines);
	  throw new InvalidInstructionException();
}

//Test invalid LDR instruction with assign pc with negative number
@Test(expected = InvalidInstructionException.class)
public void TestEC11() throws Throwable
{
	  final List<String> lines = readInstructions("examples/ec11.s");  
	  m.execute(lines);
	  throw new InvalidInstructionException();
}

//Test valid input without a JZ instruction
@Test public void TestEC12()
{
	  final List<String> lines = readInstructions("examples/ec12.s");  
	  m.execute(lines);
}

//Test valid input with Ra holds value 0
@Test public void TestEC13()
{
	  final List<String> lines = readInstructions("examples/ec13.s");  
	  m.execute(lines);
}

//Test invalid JZ input with pc over the length of the list
@Test(expected = NoReturnValueException.class)
public void TestEC14() throws Throwable
{
	  final List<String> lines = readInstructions("examples/ec14.s");  
	  m.execute(lines);
	  throw new NoReturnValueException();
}

//Test valid JZ input with Ra is not 0
@Test public void TestEC15() throws Throwable
{
	  final List<String> lines = readInstructions("examples/ec15.s");  
	  m.execute(lines);
}

//Test invalid LDR input with Rb doesn't hold any value
@Test(expected = NoReturnValueException.class)
public void TestEC16() throws Throwable
{
	  final List<String> lines = readInstructions("examples/ec16.s");  
	  m.execute(lines);
	  throw new NoReturnValueException();
}
//Test invalid instruction
@Test(expected = InvalidInstructionException.class)
public void TestEC17() throws Throwable
{
	  final List<String> lines = readInstructions("examples/ec17.s");  
	  m.execute(lines);
	  throw new InvalidInstructionException();
}

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
